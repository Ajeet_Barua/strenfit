//
//  TabBarViewController.m
//  STERNFIT
//
//  Created by Macwaves on 3/18/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "TabBarViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface TabBarViewController ()

@end

@implementation TabBarViewController


#pragma mark-ViewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"LogOut" style:UIBarButtonItemStyleBordered target:self action:@selector(LogOut:)];
    self.navigationItem.rightBarButtonItem = barButton;
}

#pragma mark - LOGOUT
- (void)LogOut:(id)sender{
    [FBSession.activeSession closeAndClearTokenInformation];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"accessToken"];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
