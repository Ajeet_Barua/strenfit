//
//  NearByViewCell.m
//  STERNFIT
//
//  Created by Macwaves on 3/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "NearByViewCell.h"

@implementation NearByViewCell
@synthesize profileImg,statusImage,add;

-(void)awakeFromNib{
    
    profileImg.layer.cornerRadius = profileImg.frame.size.width / 2.0;
    profileImg.layer.borderWidth = 2.0f;
    profileImg.layer.borderColor = [UIColor whiteColor].CGColor;
    profileImg.clipsToBounds = YES;
    add.layer.cornerRadius = add.frame.size.width / 2.0;
    add.layer.borderColor = [UIColor whiteColor].CGColor;
    add.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
