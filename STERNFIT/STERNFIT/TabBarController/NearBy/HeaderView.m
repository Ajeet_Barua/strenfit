//
//  HeaderView.m
//  STERNFIT
//
//  Created by Macwaves on 3/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "HeaderView.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation HeaderView

@synthesize ProfImg,firstName;

-(void)awakeFromNib{
    
    [self userDetails];
    [self headerView];
}

-(void)headerView{
    ProfImg.layer.cornerRadius = ProfImg.frame.size.width / 2.0;
    ProfImg.layer.borderColor = [UIColor whiteColor].CGColor;
    ProfImg.clipsToBounds = YES;
    UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    headerview.backgroundColor=[UIColor darkGrayColor];
    UIView *SearchView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    SearchView.backgroundColor=[UIColor blackColor];
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(8, 0, 70, 30)];
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    label.text=@"Near by";
    [SearchView addSubview:label];
    [headerview addSubview:SearchView];
}

-(void)userDetails{
    
    NSUserDefaults *accessTok=[NSUserDefaults standardUserDefaults];
    NSString *myString = [accessTok valueForKey:@"accessToken"];
    if (!(myString==nil)) {
    NSString *plistFilePath  = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"UserInfo.plist"];
    NSDictionary *list = [NSDictionary dictionaryWithContentsOfFile:plistFilePath];
    firstName.text=[list valueForKey:@"name"];
    NSURLSessionConfiguration *sessionConfig =[NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [list valueForKey:@"id"]]];
    [[session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"%@", [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]);
    }] resume];
    self.ProfImg.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    }
}

// get Mine Friends





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
