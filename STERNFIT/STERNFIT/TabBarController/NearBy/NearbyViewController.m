//
//  NearbyViewController.m
//  STERNFIT
//
//  Created by Macwaves on 3/12/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "NearbyViewController.h"
#import "NearByViewCell.h"
#import "HeaderView.h"

@interface NearbyViewController (){
    NSArray *userInformation;
}

@property(nonatomic,strong) IBOutlet UITableView *table;

@end

@implementation NearbyViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=NO;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"UserInfoList" ofType:@"plist"];
    userInformation = [[NSArray alloc] initWithContentsOfFile:path];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
- (NearByViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    NearByViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:1];
    textLabel.text =  [[userInformation objectAtIndex:indexPath.row] objectForKey:@"Name"];
    UILabel *textLabel2 = (UILabel *)[cell viewWithTag:2];
    textLabel2.text =  [[userInformation objectAtIndex:indexPath.row] objectForKey:@"Status"];
    UILabel *textLabel3 = (UILabel *)[cell viewWithTag:3];
    textLabel3.text =  [[userInformation objectAtIndex:indexPath.row] objectForKey:@"Time"];
    UILabel *textLabel4 = (UILabel *)[cell viewWithTag:4];
    textLabel4.text =  [[userInformation objectAtIndex:indexPath.row] objectForKey:@"Distance"];
    cell.profileImg.image = [UIImage imageNamed:[[userInformation objectAtIndex:indexPath.row] objectForKey:@"Profile Pic"]];
    [self.table setSeparatorColor:[UIColor clearColor]];
    return cell;
}

- (HeaderView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     HeaderView *header = [[[NSBundle mainBundle]  loadNibNamed:@"HeaderView" owner:self options:nil]objectAtIndex:0];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
        return 280;
}

@end
