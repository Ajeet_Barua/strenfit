//
//  FacebookLoginHandler.m
//  Benerbij
//
//  Created by Triffort on 16/01/14.
//  Copyright (c) 2014 Triffort Technologies Pvt Ltd. All rights reserved.
//

#import "FacebookLoginHandler.h"

@implementation FacebookLoginHandler
@synthesize facebook;

static FacebookLoginHandler *_shared = nil;

+(FacebookLoginHandler *)shared{
    
    @synchronized([FacebookLoginHandler class])
	{
		if (!_shared)
            
			_shared = [[FacebookLoginHandler alloc] init];
	}
    
	return _shared;
}
- (id)init
{
    self = [super init];
        
    NSString *facebookAPPId = [NSString stringWithFormat:@"%@",FACEBOOK_APP_ID];
    
    if (!self.facebook) {
        
        self.facebook = [[Facebook alloc] initWithAppId:facebookAPPId andDelegate:self];
        self.facebook.sessionDelegate = self;
    }
    return self;
}

- (void) makeLogin {
    
    NSArray *permission = @[@"user_about_me"];
    [self.facebook authorize:permission];
    
}

#pragma  mark - User Info methods

- (void) getUserInfo{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"first_name,last_name,email,picture.type(large)",@"fields",nil];
    [self.facebook requestWithGraphPath:@"me" andParams:params andDelegate:self];
    
}

- (void) getUserFriendList{
    
    NSString *graphPath = [NSString stringWithFormat:@"me/friends?fields=id,first_name,last_name,picture.type(large)"];
    [self.facebook requestWithGraphPath:graphPath andDelegate:self];
}

#pragma mark - FBSession Delegate

- (void)fbDidLogin{
    

       
    NSLog(@"Login succesfully");
  
    [self getUserInfo];
}

- (void)fbDidNotLogin:(BOOL)cancelled{
    
    NSLog(@"Could not Login succesfully");
    
}

- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt{
    
    NSLog(@"Acces token - %@",accessToken);
}

- (void)fbDidLogout{
    
    
    [DBManager makeDatabaseEmpty];
    
    [FBSession.activeSession closeAndClearTokenInformation];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate.tabBarController dismissViewControllerAnimated:YES completion:NULL];

}
- (void)fbSessionInvalidated{
    
}

#pragma mark - FBRequest Delegate

- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    
    NSLog(@"Inside didReceiveResponse: received response");
}

- (void)request:(FBRequest *)request didLoad:(id)result {
    
    NSLog(@"Inside didLoad");
    
    if ([result isKindOfClass:[NSArray class]]) {
        
        result = [result objectAtIndex:0];
        self.facebookProfileDetails = [[NSDictionary alloc] initWithDictionary:result];
    }
    
    // When we ask for user infor this will happen.
    if ([result isKindOfClass:[NSDictionary class]]){
        
        if ([[result valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            
            NSArray *fbfriendsArr = [result valueForKey:@"data"];
            self.fbFriendsArr = [[NSMutableArray alloc] init];
            
            for (NSDictionary *dict in fbfriendsArr) {
                
                NSString *firstName   = [dict objectForKey:@"first_name"];
                NSString *lastName    = [dict objectForKey:@"last_name"];
                NSString *fbID        = [dict objectForKey:@"id"];
                NSString *pictureUrl  = [[[dict objectForKey:@"picture"] objectForKey:@"data"] valueForKey:@"url"];
                
                NSDictionary *profileDetails = [[NSDictionary alloc] initWithObjectsAndKeys:fbID,kParamFbID,firstName,kParamFirstName,lastName,kParamLastName,pictureUrl,kParamImageURL, nil];
                
                [self.fbFriendsArr addObject:profileDetails];
                
            }
            
            [DBManager insertFbFriendsIntoTable:self.fbFriendsArr];
            
            NSArray *fbIdArray = [DBManager getAllFbIdsOfFbFriends];
            [APIManager getAppUsers:fbIdArray];
        }
        else{
            
            self.facebookProfileDetails = [[NSDictionary alloc] initWithDictionary:result];
            
            NSString *firstName   = [self.facebookProfileDetails objectForKey:@"first_name"];
            NSString *lastName    = [self.facebookProfileDetails objectForKey:@"last_name"];
            NSString *fbID        = [self.facebookProfileDetails objectForKey:@"id"];
            NSString *pictureUrl  = [[[self.facebookProfileDetails objectForKey:@"picture"] objectForKey:@"data"] valueForKey:@"url"];
            
            NSString *deviceToken = UDValue(DEVICE_TOKEN);
            
            UDSetValue(USER_FIRST_NAME, firstName);
            UDSetValue(USER_LAST_NAME, lastName);
            UDSetValue(USER_FB_ID, fbID);
            UDSetValue(USER_IMAGE_URL, pictureUrl);
            
            NSDictionary *facebookProfile = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             firstName,    kParamFirstName
                                             ,lastName,    kParamLastName
                                             ,fbID,        kParamFbID
                                             ,pictureUrl,  kParamImageURL
                                             ,deviceToken, kParamDeviceToken, nil];
            
            [APIManager registerWithFacebookProfileAndDeviceToken:facebookProfile];
            
            [self getUserFriendList];
        }
    }
};

/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    //[self.label setText:[error localizedDescription]];
};



@end
