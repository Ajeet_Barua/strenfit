//
//  NearByViewCell.h
//  STERNFIT
//
//  Created by Macwaves on 3/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearByViewCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UIImageView *profileImg,*statusImage,*add;

@end
