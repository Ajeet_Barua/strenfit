//
//  RegisterViewController.m
//  STERNFIT
//
//  Created by Macwaves on 3/5/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.

#import "RegisterViewController.h"
#import "CustomUIView.h"
#import "CKCalendarView.h"

@interface RegisterViewController () <CKCalendarDelegate>

@property(nonatomic,strong) IBOutlet CustomImageView *userName,*password,*emailid,*date;

@property(nonatomic,strong) IBOutlet CustomRegisterView *Name,*pass,*email,*dat;

@property(nonatomic, strong) IBOutlet UITextField *dateLabel;

@property(nonatomic, weak) CKCalendarView *calendar;

@property(nonatomic, strong) NSDateFormatter *dateFormatter;

@property(nonatomic, strong) NSDate *minimumDate;

@property(nonatomic, strong) NSArray *disabledDates;

@property(nonatomic,strong) IBOutlet UITextField *textfield1,*textfield2,*textfield3,*textfield4;

@property(nonatomic,strong) IBOutlet UIButton *b1,*b2;


@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
 }

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Action
-(IBAction)calendar:(id)sender{
    CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = calendar;
    calendar.delegate = self;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ){
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        if( screenHeight < screenWidth ){
            screenHeight = screenWidth;
        }
        if( screenHeight > 480 && screenHeight < 667 ){
            NSLog(@"iPhone 5/5s");
             calendar.frame = CGRectMake(20, 0, 280, 400);
            [self calenderAction];
        } else if ( screenHeight > 480 && screenHeight < 736 ){
            calendar.frame = CGRectMake(30, 20, 320, 504);
            [self calenderAction];
            NSLog(@"iPhone 6");
            } else if ( screenHeight > 480 ){
                calendar.frame = CGRectMake(45, 122, 320, 504);
                [self calenderAction];
            NSLog(@"iPhone 6 Plus");
        } else {
            calendar.frame = CGRectMake(60, 20, 200, 280);
            [self calenderAction];
            NSLog(@"iPhone 4/4s");
        }
    }
}

-(IBAction)selectGender:(id)sender{
    if([sender tag] == 101){
   _b1.backgroundColor=[UIColor redColor];
   _b2.backgroundColor=[UIColor clearColor];
    }
    else if ([sender tag] == 102){
        _b2.backgroundColor=[UIColor redColor];
        _b1.backgroundColor=[UIColor clearColor];
    }
}

-(void)calenderAction{
    [self.view addSubview:_calendar];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    self.dat.userInteractionEnabled=NO;
}

#pragma mark - CKCalendarDelegate
- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    if ([self dateIsDisabled:date]) {
        dateItem.backgroundColor = [UIColor redColor];
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date {
    for (NSDate *disabledDate in self.disabledDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
   
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    self.dateLabel.text = [self.dateFormatter stringFromDate:date];
    self.calendar.hidden=YES;
    self.dat.userInteractionEnabled=YES;
    self.email.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
        return YES;
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
}

#pragma mark -- Touch Began Method

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self.calendar removeFromSuperview];
    self.dat.userInteractionEnabled=YES;
    self.email.userInteractionEnabled=YES;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
}

#pragma mark -- TextField Delegates method

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == _textfield1) {
        [self textfieldBegin];
    } else if (textField == _textfield2) {
        [self textfieldBegin];
    }
    else if (textField == _textfield3) {
        [self textfieldBegin];
    }
    else if (textField == _textfield4) {
        [self textfieldBegin];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField == _textfield1) {
        [self textFieldEnd];
    } else if (textField == _textfield2) {
        [self textFieldEnd];
    }
    
    else if (textField == _textfield3) {
        [self textFieldEnd];
    }
    else if (textField == _textfield4) {
        [self textFieldEnd];
    }
}

-(void)textFieldEnd{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 100.0), self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textfieldBegin{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 100.0), self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}


@end
