//
//  SignInViewController.h
//  STERNFIT
//
//  Created by Macwaves on 3/5/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CustomImageView.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,FBLoginViewDelegate>

-(IBAction)facebookLogin:(id)sender;
-(IBAction)Login:(id)sender;

@end
