//
//  RegisterViewController.h
//  STERNFIT
//
//  Created by Macwaves on 3/5/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomImageView.h"
#import "CustomRegisterView.h"

@interface RegisterViewController : UIViewController

-(IBAction)calendar:(id)sender;
-(IBAction)selectGender:(id)sender;

@end
