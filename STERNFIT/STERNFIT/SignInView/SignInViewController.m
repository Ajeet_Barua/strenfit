//
//  SignInViewController.m
//  STERNFIT
//
//  Created by Macwaves on 3/5/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "SignInViewController.h"
#import "TabBarViewController.h"


@interface SignInViewController ()<FBFriendPickerDelegate>{
    BOOL flag;
}
   
@property(nonatomic,strong) IBOutlet CustomButton *signUp,*login,*facebookLogin;

@property(nonatomic,strong) IBOutlet CustomImageView *userNameImage,*passwrdImage;

@property(nonatomic,strong) IBOutlet UITextField *userName,*passwrd;

@property(nonatomic,strong) IBOutlet UITextField *textfield1,*textfield2;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    _signUp.layer.cornerRadius=8.0f;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark---LOGIN METHOD

-(IBAction)Login:(id)sender{
    [self.view endEditing:YES];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    TabBarViewController *tabBarView = (TabBarViewController*)[mainStoryboard
                                                               instantiateViewControllerWithIdentifier: @"TabBarViewController"];
    UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:tabBarView];
     [nav.navigationBar setBarTintColor:[UIColor blackColor]];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

-(IBAction)facebookLogin:(id)sender{
   
    if (FBSession.activeSession.isOpen ) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"email",@"user_photos",@"user_birthday",@"read_friendlists", nil];
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error){
            if(!error) {
                [[FBRequest requestForMe] startWithCompletionHandler:
                 ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                    if (!error) {
                    [self fetchFriends:^(NSArray *friends) {
                            NSLog(@"%@", friends);
                        }];
                    NSArray *friendList=[user valueForKey:@"data"];
                    NSLog(@"%@",friendList);
                     NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                     path = [path stringByAppendingPathComponent:@"UserInfo.plist"];
                     [user writeToFile:path atomically:YES];
                     if (!error) {
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setObject:[[[FBSession activeSession] accessTokenData]accessToken] forKey:@"accessToken"];
                         [defaults synchronize];
                         UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                  bundle: nil];
                         TabBarViewController *tabBarView = (TabBarViewController*)[mainStoryboard
                                                                                           instantiateViewControllerWithIdentifier: @"TabBarViewController"];
                          UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tabBarView];
                          [nav.navigationBar setBarTintColor:[UIColor blackColor]];
                         [self.navigationController presentViewController:nav animated:YES completion:nil];
                    }
                     }
                 }];
            } else {
                NSLog(@"Error : %@",error);
            }
        }];
    }
}

//get friendList

- (void)fetchFriends:(void(^)(NSArray *friends))callback
{
    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id response, NSError *error) {
        NSMutableArray *friends = [NSMutableArray new];
        if (!error) {
            [friends addObjectsFromArray:(NSArray*)[response data]];
        }
        callback(friends);
    }];
}



#pragma mark---textField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == _textfield1) {
        [self textfieldBegin];
    } else if (textField == _textfield2) {
        [self textfieldBegin];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField == _textfield1) {
        [self textFieldEnd];
    } else if (textField == _textfield2) {
        [self textFieldEnd];
    }
}

-(void)textFieldEnd{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 100.0), self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}
-(void)textfieldBegin{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 100.0), self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}



@end
