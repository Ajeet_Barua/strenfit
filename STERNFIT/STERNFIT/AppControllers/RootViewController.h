//
//  ViewController.h
//  STERNFIT
//
//  Created by Macwaves on 3/5/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface RootViewController : UIViewController

 - (IBAction)changePage:(id)sender;

@end

