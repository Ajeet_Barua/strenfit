//
//  CircleButton.m
//  STERNFIT
//
//  Created by Macwaves on 3/24/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import "CircleButton.h"

@implementation CircleButton

-(void)awakeFromNib{
    self.layer.cornerRadius = self.frame.size.width / 2.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 2.0f;
    self.clipsToBounds = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
