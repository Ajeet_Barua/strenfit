//
//  HeaderView.h
//  STERNFIT
//
//  Created by Macwaves on 3/20/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView{
    NSMutableArray *friendsArray;
}


@property(nonatomic,strong) IBOutlet UIImageView *ProfImg;

@property(weak,nonatomic) IBOutlet UILabel *firstName;

@end
