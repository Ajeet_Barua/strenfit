//
//  ViewController.m
//  STERNFIT
//  Created by Macwaves on 3/5/15.
//  Copyright (c) 2015 Macwaves. All rights reserved.


#import "RootViewController.h"
#import "TabBarViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface RootViewController (){
    NSArray *imageArray;
}

#pragma mark - Property Declaration
@property(nonatomic,strong) IBOutlet CustomButton *SignIn,*Register;

@property(nonatomic,strong) IBOutlet UIScrollView *scrollView;

@property(nonatomic,strong) IBOutlet UIPageControl *pageControl;

@end

@implementation RootViewController
@synthesize scrollView,pageControl;


#pragma mark - DidLoad Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageContrl];
    [self loadingView];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)loadingView{
    
    NSUserDefaults *accessTok=[NSUserDefaults standardUserDefaults];
    NSString *myString = [accessTok valueForKey:@"accessToken"];
    if (!(myString==nil)) {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        TabBarViewController *tabBarView = (TabBarViewController*)[mainStoryboard
                                                                   instantiateViewControllerWithIdentifier: @"TabBarViewController"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:tabBarView];
        [nav.navigationBar setBarTintColor:[UIColor blackColor]];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

#pragma mark - Button Action
- (IBAction)changePage:(id)sender {
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    int page = scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);
    pageControl.currentPage = page;
}

#pragma mark - PageControl Action
-(void)pageContrl{
    
    pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    imageArray = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"Front.jpg"], [UIImage imageNamed:@"Front.jpg"], [UIImage imageNamed:@"Front.jpg"],[UIImage imageNamed:@"Front.jpg"],[UIImage imageNamed:@"Front.jpg"], nil];
    for (int i = 0; i < imageArray.count; i++) {
        CGRect frame;
        frame.origin.x = self.scrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.scrollView.frame.size;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [imageArray objectAtIndex:i];
        [self.scrollView addSubview:imageView];
    }
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * [imageArray count], 200);
}


@end
