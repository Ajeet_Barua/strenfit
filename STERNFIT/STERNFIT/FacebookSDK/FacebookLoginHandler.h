//
//  FacebookLoginHandler.h
//  Benerbij
//
//  Created by Triffort on 16/01/14.
//  Copyright (c) 2014 Triffort Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacebookLoginHandler : NSObject<FBSessionDelegate,FBRequestDelegate,FBDialogDelegate>

@property (strong, nonatomic) Facebook *facebook;
@property (strong, nonatomic) NSDictionary   *facebookProfileDetails;
@property (strong, nonatomic) NSMutableArray *fbFriendsArr;
@property (nonatomic, retain) LoginViewController *loginViewController;

+ (FacebookLoginHandler *)shared;
- (void) makeLogin ;
- (void) getUserFriendList;
@end
